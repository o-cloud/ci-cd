# CI-CD

Containt all gitlab CI/CD templates

```mermaid
flowchart LR
  subgraph Test
    direction LR
    subgraph TestGo
        direction LR
        ttgt("commit")
        ttgd["Run go test coverage"]
    end
    subgraph TestHelm
        direction LR
        ttht("commit")
        tthd["Verify Helm chart"]
    end
  end
  subgraph Build
    subgraph BuildDocker
        bbdt("main, tag,\nmerge requests")
        bbdd["Build docker image \nand push it to gcr"]
    end
  end
  subgraph Deploy
        direction LR
    subgraph Deploy Umbrella
        direction LR
        ddut("main")
        ddud["Deploy service using\numbrella chart"]
    end
    subgraph Deploy Standalone
        direction LR
        ddst("main")
        ddsd["Deploy service using\nservice chart"]
    end
  end
  subgraph Publish
        direction LR
    subgraph Tag Latest
        direction LR
        ttlt("main")
        ttld["Add tag Latest\nto built image"]
    end
    subgraph Tag Image Version
        direction LR
        tttt("tag")
        tttd["Add tag version\nto built image"]
    end
    subgraph Tag Helm Version
        direction LR
        tht("tag")
        thd["Change version of helm\nand image then push\nchart to registry"]
    end
  end
  Test --> Build
  Build --> Publish
  Publish --> Deploy
```
# Stages

Currently 4 stages

* Test
* Build
* Publish
* Deploy

## Test

This stage regroup jobs whose objective is to test code

### Jobs
#### Go Tests
When:
* Run on every pushed commit

Run go tests and upload test coverage as artifact 

#### helm lint
When:
* Run on merge request when helm files are changed

Run helm lint to check if files syntax is valid
## Build

This stage regroup jobs whose objective is to compile code into executable and/or docker image

### Jobs

#### Build container image
When: 
* On every commit of master
  
Build a docker image using kaniko with current commit short SHA as tag
It should reuse cache and not rebuild image everytime even if job run again on same commit

## Deploy

This stage regroup jobs whose objective is to deploy applications to  the integration cluster

### Jobs

#### deploy
When: 
* On every commit of master

Deploy build image with current commit slug as tag using helm repository to kubernetes cluster

Variables can be used to configure helm parameters (namespace for example)

## Tag 

### Jobs 

#### 
#### Build helm
When: 
* On every tag
Change helm chart version, freeze image version, package helm chart and push it to helm repository from master commit

#### Retag container image latest
When:
* On every commit of master after build image success

Use gcloud console to add latest tag to image

#### Retag container image with git tag
When:
* On every commit of master after build image success

Use gcloud console to add git tag to image
